﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmpWeb.aspx.cs" Inherits="TablesWebForm.EmpWeb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 225px;
            margin-left: 320px;
        }
        .auto-style2 {
            width: 89px;
        }
        .auto-style3 {
            width: 89px;
            height: 23px;
        }
        .auto-style4 {
            height: 23px;
        }
        .auto-style5 {
            width: 89px;
            height: 26px;
        }
        .auto-style6 {
            height: 26px;
        }
    </style>
    <h1 class="auto-style1">Employee Form</h1>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Employee Form Fill up</div>
        <table style="width:100%;">
            <tr>
                <td class="auto-style5">EMPNO</td>
                <td class="auto-style6">
                    <asp:TextBox ID="Txtempno" runat="server" Width="160px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*This is a number " ControlToValidate="Txtempno" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td class="auto-style6"></td>
            </tr>
            <tr>
                <td class="auto-style3">ENAME</td>
                <td class="auto-style4">
                    <asp:TextBox ID="Txtename" runat="server" Width="160px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*name is manditory" ControlToValidate="Txtename" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td class="auto-style4"></td>
            </tr>
            <tr>
                <td class="auto-style2">JOB</td>
                <td>
                    <asp:TextBox ID="Txtjob" runat="server" Width="161px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*Job titile" ControlToValidate="Txtjob" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">MGR</td>
                <td>
                    <asp:TextBox ID="Txtmgr" runat="server" Width="160px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*Manager Code" ControlToValidate="Txtmgr" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">HIREDATE</td>
                <td>
                    <asp:TextBox ID="Txtdate" runat="server" Width="160px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*Date" ControlToValidate="Txtdate" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">SAL</td>
                <td>
                    <asp:TextBox ID="Txtsal" runat="server" Width="160px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*This an number " ControlToValidate="Txtsal" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">COMM</td>
                <td>
                    <asp:TextBox ID="Txtcomm" runat="server" Width="161px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*commission" ControlToValidate="Txtcomm" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">DEPTNO</td>
                <td>
                    <asp:TextBox ID="Txtdeptno" runat="server" Width="160px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*Deptnumber" ControlToValidate="Txtdeptno" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <asp:Button ID="Button1" runat="server" Font-Bold="True" OnClick="Button1_Click" Text="Submit" />
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Address.aspx.cs" Inherits="TablesWebForm.Address" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 354px;
            margin-left: 320px;
        }
        .auto-style2 {
            width: 100%;
            height: 107px;
        }
        .auto-style3 {
            width: 100px;
        }
        .auto-style4 {
            width: 336px;
        }
        .auto-style5 {
            width: 148px;
        }
        .auto-style6 {
            width: 100px;
            height: 26px;
        }
        .auto-style7 {
            width: 148px;
            height: 26px;
        }
        .auto-style8 {
            width: 336px;
            height: 26px;
        }
    </style>
    <h1 class="auto-style1">This Address Table Form </h1>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p>Address Form Fill up</p>
        </div>

        <table class="auto-style2">
            <tr>
                <td class="auto-style6">Address_ID</td>
                <td class="auto-style7">
                    <asp:TextBox ID="txtAddressID" runat="server" Width="266px"></asp:TextBox>
                </td>
                <td class="auto-style8">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAddressID" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Street</td>
                <td class="auto-style5">
                    <asp:TextBox ID="txtStreet" runat="server" Width="266px"></asp:TextBox>
                </td>
                <td class="auto-style4">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCity" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">City</td>
                <td class="auto-style5">
                    <asp:TextBox ID="txtCity" runat="server" Width="266px"></asp:TextBox>
                </td>
                <td class="auto-style4">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEMPno" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">stateid</td>
                <td class="auto-style5">
                    <asp:TextBox ID="txtstateid" runat="server" Width="266px"></asp:TextBox>
                </td>
                <td class="auto-style4">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtstateid" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Zipcode</td>
                <td class="auto-style5">
                    <asp:TextBox ID="txtZipcode" runat="server" Width="266px"></asp:TextBox>
                </td>
                <td class="auto-style4">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtZipcode" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style6">EMPNO</td>
                <td class="auto-style7">
                    <asp:TextBox ID="txtEMPno" runat="server" Width="266px"></asp:TextBox>
                </td>
                <td class="auto-style8">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEMPno" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
          
        </table>
        <p>
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="submit" />
        </p>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DepWebForm.aspx.cs" Inherits="TablesWebForm.DepWebForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 253px;
            margin-left: 320px;
        }
        .auto-style2 {
            width: 242px;
        }
        .auto-style3 {
            width: 292px;
        }
    </style>
        <div>
            please the department form
        </div>

    <h1 class="auto-style1">Department Form</h1>
</head>
<body>
    <form id="form1" runat="server">
        <table style="width:100%;">
            <tr>
                <td class="auto-style2">DEPTNO</td>
                <td class="auto-style3">
                    <asp:TextBox ID="Txtdeptno" runat="server" OnTextChanged="TextBox1_TextChanged" Width="200px"></asp:TextBox>
&nbsp;</td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="Txtdeptno"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">DEPName</td>
                <td class="auto-style3">
                    <asp:TextBox ID="Txtdepname" runat="server" Width="200px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Txtdepname" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">LOCATION</td>
                <td class="auto-style3">
                    <asp:TextBox ID="Txtloc" runat="server" Width="200px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Txtloc" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Submit" />
                </td>
                <td class="auto-style3">
                    <asp:Button ID="btnEmp" runat="server" OnClick="btnEmp_Click" Text="Employee" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        

    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TABLES;

namespace TablesWebForm
{
    public partial class DepWebForm : System.Web.UI.Page
    {
        List<Dept> depts;
        protected void Page_Load(object sender, EventArgs e)
        {
            //List<Dept> depts = new List<Dept>();


        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            HttpCookie objCookie = new HttpCookie("varName");
            if (Session["records"] == null)
            {
               depts = new List<Dept>();
                

                objCookie.Values.Add("first", "one");
                objCookie.Values.Add("Second", "Two");
                objCookie.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(objCookie);
                string TempValue = objCookie.Values["first"];
            }
            else
            {
                depts = (List<Dept>)Session["records"];
                HttpCookie objCookie1 = Request.Cookies["varName"];
                string TempValue = objCookie1.Values["first"];
            }

            Dept dept = new Dept();
            dept.DEPTNO=Convert.ToInt32(Txtdeptno.Text);
            dept.DNAME=Txtdepname.Text;
            dept.LOC=Txtloc.Text;
            
            depts.Add(dept);
            Session["records"] = depts;

        }

        protected void btnEmp_Click(object sender, EventArgs e)
        {
           
            Response.Redirect("EmpWeb.aspx?Dno="+ Txtdeptno.Text +"&Name="+Txtdepname.Text);
        }
    }
}
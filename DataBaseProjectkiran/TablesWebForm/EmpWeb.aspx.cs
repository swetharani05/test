﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TABLES;

namespace TablesWebForm
{
    public partial class EmpWeb : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Dept> depts = (List<Dept>)Session["records"];
            int Deptno =Convert.ToInt32( Request.QueryString["dno"]);
            string Dname = Request.QueryString["Name"];

            HttpCookie objCookie = Request.Cookies["varName"];
            string TempValue = objCookie.Values["first"];
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Employee employee=new Employee();
            employee.EMPNO = Txtempno.Text;
            employee.ENAME = Txtename.Text;
            employee.JOB=Txtjob.Text;
            employee.MGR=Txtmgr.Text;
            employee.HIREDATE = Convert.ToDateTime(Txtdate.Text);
            employee.SAL = Convert.ToInt32(Txtsal.Text);
            employee.COMM = Convert.ToInt32(Txtcomm.Text);
            employee.DEPTNO=Txtdeptno.Text;
        }
    }
}
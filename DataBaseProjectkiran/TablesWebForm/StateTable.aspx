﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StateTable.aspx.cs" Inherits="TablesWebForm.StateTable" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <h1 class="auto-style3">State Table Web Form</h1>
    <style type="text/css">
        .auto-style1 {
            width: 100px;
        }
        .auto-style2 {
            width: 302px;
        }
        .auto-style3 {
            width: 320px;
            height: 38px;
            margin-left: 360px;
        }
        .auto-style4 {
            width: 100px;
            height: 26px;
        }
        .auto-style5 {
            width: 302px;
            height: 26px;
        }
        .auto-style6 {
            height: 26px;
        }
    </style>
</head>
<body style="height: 237px">
    <form id="form1" runat="server">
        <div>
            State Table Forms
        </div>
        <table style="width: 100%;">
            <tr>
                <td class="auto-style4">stateid</td>
                <td class="auto-style5">
                    <asp:TextBox ID="txtStateID" runat="server"></asp:TextBox>
                </td>
                <td class="auto-style6">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtStateID" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">state_code</td>
                <td class="auto-style2">
                    <asp:TextBox ID="TxtStateCode" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TxtStateCode" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">state_name</td>
                <td class="auto-style2">
                    <asp:TextBox ID="TxtStateName" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TxtStateName" ErrorMessage="*" ForeColor="#FF3300"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
        <asp:Button ID="BtnSubmitt" runat="server" BorderStyle="Solid" OnClick="Button1_Click" Text="Submitt" Width="127px" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="#FF3300" />
    </form>
    <p>
        `</p>
</body>
</html>

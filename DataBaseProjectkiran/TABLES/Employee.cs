﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TABLES
{
    public class Employee
    {
        public string EMPNO { get; set; }
        public string ENAME { get; set; }

        public string JOB { get; set; }

        public string MGR { get; set; }

        public DateTime HIREDATE { get; set; }

        public Int32 SAL { get; set;}

        public Int32 COMM { get; set; } 

        public string DEPTNO { get; set;}

        public void EmpDetails()
        {
            Console.WriteLine("Enter EMPNO:");
            EMPNO=Console.ReadLine();
            Console.WriteLine("ENTER ENAME:");
            ENAME=Console.ReadLine();
            Console.WriteLine("ENTER THE JOB:");
            JOB=Console.ReadLine();
            Console.WriteLine("ENTER THE MGR CODE:");
            MGR=Console.ReadLine();
            Console.WriteLine("ENTER HIREDATE:");
            HIREDATE = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("ENTER THE SAL :");
            SAL=Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("ENTER THE COMMISION:");
            COMM=Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("ENTER THE DEPT NO :");
            DEPTNO=Console.ReadLine();

            ArrayList EM = new ArrayList();
            EM.Add(EMPNO);
            EM.Add(ENAME);
            EM.Add(JOB);
            EM.Add(HIREDATE);
            EM.Add(SAL);
            EM.Add(COMM);
            EM.Add(DEPTNO);
            foreach (var v in EM)
            {
                Console.WriteLine("The data is "+v);
                Console.ReadLine();
            }
        }
    }
}

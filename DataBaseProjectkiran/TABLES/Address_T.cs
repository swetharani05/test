﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TABLES
{
    public class Address_T
    {
        public int Address_ID { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string stateid { get; set; }
        public string ZipCode { get; set; }
        public string EMPNO { get; set; }

        public void DB_Address()
        {
            
            Console.WriteLine("Enter addressid here:");
            int address_ID = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter the street name:");
            Street =Console.ReadLine();
            Console.WriteLine("Enter the city:");
            City = Console.ReadLine();
            Console.WriteLine("Enter the stateid: ");
            stateid = Console.ReadLine();
            Console.WriteLine("Enter the Zipcode: ");
            ZipCode = Console.ReadLine();
            Console.WriteLine("Enter the EMPNO:");
            EMPNO = Console.ReadLine();

            ArrayList AL = new ArrayList();
            AL.Add(address_ID);
            AL.Add(Street);
            AL.Add(City);
            AL.Add(stateid);
            AL.Add(ZipCode);
            AL.Add(EMPNO);

            foreach(var v in AL)
            {
                Console.WriteLine("The data is "+v);
                Console.ReadLine();
            }
        }

    }
}

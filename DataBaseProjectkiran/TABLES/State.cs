﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TABLES
{
    public class State
    {
        public int stateid { get; set; }
        public string state_code { get; set; }

        public string state_name { get; set;}

        public void StateDetails()
        {
            Console.WriteLine("enter the state id:");
            stateid=Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter the state_code:");
            state_code=Console.ReadLine();
            Console.WriteLine("enter the state_name:");
            state_name=Console.ReadLine();

            ArrayList statelist= new ArrayList();
            statelist.Add(stateid);
            statelist.Add(state_code);
            statelist.Add(state_name);

            foreach (var v in statelist) 
            {
                Console.WriteLine("The data is " + v);
                Console.ReadLine();
            }
        }
    }
}
